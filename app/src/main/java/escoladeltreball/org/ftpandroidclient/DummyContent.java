package escoladeltreball.org.ftpandroidclient;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DummyContent {

    public static final List<DummyItem> ITEMS = new ArrayList<DummyItem>();


    public static  String[] Array_ITEMS = new String[]{"foto.jpg", "document.doc", "video.wav"};


    public static  int x = 1;

    public static final Map<String, DummyItem> ITEM_MAP = new HashMap<String, DummyItem>();

    private static final int COUNT = 150;



    static {
        // Add some sample items.
       // for (int i = 0; i < Array_ITEMS.length; i++) {
       //     addItem(createDummyItem(Array_ITEMS[i]));
       // }
    }


    public static void setItemsList(String[] items){
        ITEMS.clear();
        for(String s : items){
            addItem(createDummyItem(s));
        }
    }



    private static void addItem(DummyItem item) {
        ITEMS.add(item);
        ITEM_MAP.put(item.id, item);
    }

    private static DummyItem createDummyItem(String info) {
        return new DummyItem(String.valueOf(x++),  info);
    }


    /**
     * A dummy item representing a piece of content.
     */
    public static class DummyItem {
        public DummyItem(
                String id, String content) {

            this.id = id;
            this.content = content;
        }

        public final String id;
        public final String content;




        @Override
        public String toString() {
            return content;
        }
    }
}
