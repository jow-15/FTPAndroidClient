package escoladeltreball.org.ftpandroidclient;

import android.os.AsyncTask;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPClientConfig;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by root on 5/9/17.
 */

public class Ftp extends AsyncTask {

    private String user;
    private String pass;
    private String server;
    private int port;
    private FTPClient ftpClient;
    private FTPFile lof[] = {};
    private ArrayList files;
    private String[] ftpFiles = new String[]{};



    public Ftp(String user, int port, String server, String pass) {
        this.user = user;
        this.port = port;
        this.server = server;
        this.pass = pass;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getServer() {
        return server;
    }

    public void setServer(String server) {
        this.server = server;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    @Override
    protected Object doInBackground(Object[] params) {
        FTPClientConfig conf = new FTPClientConfig(FTPClientConfig.SYST_UNIX);
        ftpClient = new FTPClient();
        ftpClient.configure(conf);
        boolean status = false;
        try {

            ftpClient.connect(server,port);
            if (!ftpClient.login(user, pass)) {
                ftpClient.logout();
            }
            int reply = ftpClient.getReplyCode();
            if (!FTPReply.isPositiveCompletion(reply)) {
                ftpClient.disconnect();
                return new String []{"bad","bad"};
            }
            ftpClient.enterLocalPassiveMode();
            ftpFiles = ftpClient.listNames();
            if (ftpFiles != null && ftpFiles.length > 0) {
                System.out.println("AQUI");
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (ftpClient.isConnected()) {
                    ftpClient.logout();
                    ftpClient.disconnect();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }

        return ftpFiles;
    }

    public static ArrayList tractarLlistaFitxers(FTPFile[] lof){

        ArrayList files = new ArrayList();

        for(FTPFile linia:lof){
            files.add(linia.getName());
        }

        return files;
    }

    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);
    }
}