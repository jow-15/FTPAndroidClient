package escoladeltreball.org.ftpandroidclient;

import android.os.AsyncTask;

import io.swagger.client.ApiClient;
import io.swagger.client.api.LoginApi;
import io.swagger.client.model.LogUser;
import io.swagger.client.model.User;

/**
 * Created by root on 5/24/17.
 */

public class Login extends AsyncTask{

    private String nick;
    private String password;

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Login(String nick, String password){

        this.nick = nick;
        this.password = password;

    }

    @Override
    protected Object doInBackground(Object[] params) {

        try {
            LoginApi apiInstance = new LoginApi();
            ApiClient pc = new ApiClient();
            pc.setBasePath("http://10.0.2.2:8081/v1/");
            apiInstance.setApiClient(pc);
            LogUser logUser = new LogUser();
            logUser.setNick(nick);
            logUser.setPassword(password);
            User u = apiInstance.logUser(logUser);
            return u;

        } catch (Exception e) {
            e.printStackTrace();
            return null;

        }

    }

    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);
    }
}