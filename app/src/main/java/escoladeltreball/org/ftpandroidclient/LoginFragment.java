package escoladeltreball.org.ftpandroidclient;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.concurrent.ExecutionException;

import butterknife.ButterKnife;
import butterknife.InjectView;
import io.swagger.client.model.User;


public class LoginFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static boolean valid = true;
    private static String email = "";
    private static String password = "";
    private static User u;
    @InjectView(R.id.btn_login) Button _loginButton;
    @InjectView(R.id.link_signup)TextView _signupLink;
    @InjectView(R.id.input_email)EditText _emailText;
    @InjectView(R.id.input_password) EditText _passwordText;

    private String mParam1;
    private String mParam2;


    private OnLoginInteractionListener mListener;


    public LoginFragment() {
        // Required empty public constructor
    }


    public static LoginFragment newInstance(String param1, String param2) {
        LoginFragment fragment = new LoginFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if(mParam1 != null){
            if(mParam1.equals("bad")){
                Toast.makeText(getContext(), "Bad credentials", Toast.LENGTH_SHORT).show();
            }
        }
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        ButterKnife.inject(this,view);
        _loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();

            }
        });
        _signupLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    onLoginPressed("sign");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        });
        return view;
    }


    public void onLoginPressed(String type) throws InterruptedException {
        if (mListener != null) {
            mListener.onLoginInteraction(type, email,password);
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnLoginInteractionListener) {
            mListener = (OnLoginInteractionListener) context;
        }else{
            throw new RuntimeException(context.toString()
                    + " must implement OnSignUpInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;

    }


    public void login() {

        boolean sortida = true;

        if (!validateInput()) {
            onLoginFailed();
        }

        final ProgressDialog progressDialog = new ProgressDialog(getContext(),
                R.style.AppTheme_Dark_Dialog);

        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false); // Per tal de que no sigui possible saltar-se la animació
        progressDialog.setMessage(getResources().getString(R.string.autenticate));
        progressDialog.show();

        email = _emailText.getText().toString();
        password = _passwordText.getText().toString();

        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        try {
                            u = (User) new Login(email,password).execute().get();
                            if(valid){
                                    try {
                                        onLoginPressed("login");
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                        valid = false;
                                        onLoginFailed();
                                    }
                                }

                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        } catch (ExecutionException e) {
                            e.printStackTrace();
                        }
                        progressDialog.dismiss();
                    }
                }, 3000);
    }

    public void onLoginSuccess() {
        _loginButton.setEnabled(true);

    }

    public void onLoginFailed() {
        _loginButton.setEnabled(true);

    }

    public boolean validateInput() {

        email = _emailText.getText().toString();
        password = _passwordText.getText().toString();

        if (email.isEmpty()) {
            _emailText.setError(getResources().getString(R.string.badEmail));
            valid = false;
            return valid;
        } else {
            _emailText.setError(null);
        }

        valid = true;
        if(password.equals("")){
            password = null;
        }
        return valid;
    }


    public interface OnLoginInteractionListener {

        void onLoginInteraction(String type, String nick, String password) throws InterruptedException;
    }


}
