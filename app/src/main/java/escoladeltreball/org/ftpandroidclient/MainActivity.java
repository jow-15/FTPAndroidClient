package escoladeltreball.org.ftpandroidclient;

import android.Manifest;
import android.app.Activity;
import android.app.NotificationManager;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.PersistableBundle;
import android.preference.PreferenceManager;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.util.Locale;
import java.util.concurrent.ExecutionException;

import io.swagger.client.api.LoginApi;
import io.swagger.client.model.User;

public class MainActivity extends AppCompatActivity implements LoginFragment.OnLoginInteractionListener,
        SignInFragment.OnSignUpInteractionListener, DocumentFragment.OnFragmentInteractionListener,
        MyProfile.OnFragmentInteractionListener{

    private Integer FILE_SELECT_CODE = 0 ;
    private FragmentManager fragmentManager = getSupportFragmentManager();
    boolean doubleBackToExitPressedOnce = false;
    private static LoginApi apiInstance;
    private static User u;
    private static String[] listFiles;

    private static String _server = "192.168.2.33";
    private static int _port = 21;
    private static String _user;
    private static String _pass;


    public static FloatingActionButton btn_upload;
    public static final int TIPO_IMAGEN = 1;
    public static final int TIPO_AUDIO = 2;
    public static final int TIPO_DOCUMENTO = 3;
    public static final int TIPO_SISTEMA = 4;
    public static final int TIPO_DESCONOCIDO = 0;

    public static final int SHORT_CLICK = 50;
    public static final int LONG_CLICK = 100;

    public static final String TIPO_NORMAL= "isNormal";
    public static final String TIPO_PREMIUM= "isPremium";
    public static final String SAVEBACK= "save&back";
    public static final String BACK= "back";
    private boolean change_fragment=false;

    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    NotificationManager mNotifyManager;

    public NotificationCompat.Builder mBuilder =
            new NotificationCompat.Builder(this)
                    .setSmallIcon(android.R.drawable.stat_sys_download_done)
                    .setContentTitle("State of the download")
                    .setContentText("Iniciating");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
        Configuration config = getBaseContext().getResources().getConfiguration();

        mNotifyManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        String lang = settings.getString("LANG", "");
        if (! "".equals(lang) && ! config.locale.getLanguage().equals(lang)) {
            Locale locale = new Locale(lang);
            Locale.setDefault(locale);
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        }
        btn_upload = (FloatingActionButton)findViewById(R.id.btn_upload);
        btn_upload.hide();
        verifyStoragePermissions(this);
        Fragment loginFragment = LoginFragment.newInstance(null,null);
        changeFragment(loginFragment,true);
        getSupportActionBar().hide();
        verifyStoragePermissions(this);
        btn_upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fileChooser();

            }
        });

    }

    public void onActivityResult(int requestCode, int resultCode, Intent data){

        String path = "";

        if (resultCode == RESULT_OK) {
            if (requestCode == FILE_SELECT_CODE) {

                Uri uri = data.getData();
                path = getPath(this,uri);
                Toast.makeText(this.getApplicationContext(),getResources().getString(R.string.upload), Toast.LENGTH_SHORT).show();
                new Upload(_server,_port,_user,_pass,path).execute();


            }
        }

    }


    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.settings:
                getSupportActionBar().hide();
                btn_upload.hide();
                Fragment fragmentSettings =
                        MyProfile.newInstance(
                                "userName",
                                "Password",
                                TIPO_NORMAL);

                changeFragment(fragmentSettings, true);
                break;
            case R.id.logout:
                getSupportActionBar().hide();
                btn_upload.hide();
                Fragment fragmntLogin = LoginFragment.newInstance("JOW", "password");
                changeFragment(fragmntLogin, false);
                break;
            case R.id.Language_ca:
                PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().putString("LANG", "ca").commit();
                setLangRecreate("ca");
                break;
            case R.id.Language_en:
                PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().putString("LANG", "en").commit();
                setLangRecreate("en");
                break;
            case R.id.Language_es:
                PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().putString("LANG", "es").commit();
                setLangRecreate("es");
                break;
            case R.id.refresh:
                try {
                    listFiles = (String[]) new Ftp(_user,_port,_server,_pass).execute().get();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                DocumentFragment fragment = DocumentFragment.newInstance(3);
                DummyContent.setItemsList(listFiles);

                changeFragment(fragment, true);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void setLangRecreate(String langval) {
        Configuration config = getBaseContext().getResources().getConfiguration();
        Locale locale = new Locale(langval);
        Locale.setDefault(locale);
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        recreate();
    }



    private void changeFragment(Fragment fragment, boolean addToBackStack) {

        if (addToBackStack) {
            fragmentManager
                    .beginTransaction()
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE)
                    .show(fragment)
                    .replace(R.id.fragment_container, fragment)
                    .addToBackStack(null).commit();
        } else {
            fragmentManager
                    .beginTransaction()
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE)
                    .show(fragment)
                     .replace(R.id.fragment_container, fragment)
                    .commit();
        }
    }


    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            finish();
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, getResources().getString(R.string.dobleBack), Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }

    @Override
    public void onLoginInteraction(String type, String nick, String password) throws InterruptedException {

        _pass = password;
        _user=nick;
        if(type.equals("login")){
            try{
                try {
                    listFiles = (String[]) new Ftp(_user,_port,_server,_pass).execute().get();

                    getSupportActionBar().setTitle(R.string.yourFiles);
                    getSupportActionBar().show();
                    btn_upload.show();
                    DocumentFragment fragment = DocumentFragment.newInstance(3);
                    DummyContent.setItemsList(listFiles);
                    changeFragment(fragment, true);

                } catch (ExecutionException e) {
                    LoginFragment lf = LoginFragment.newInstance("bad","bad");
                    changeFragment(lf, true);
                    e.printStackTrace();
                }


            }catch (Exception e){

                Toast.makeText(this, "Failed", Toast.LENGTH_SHORT).show();
                System.out.println(e);

            }
        }else if(type.equals("sign")){
            Fragment SigFragment = SignInFragment.newInstance(null,null);
            changeFragment(SigFragment,true);
        }else if(type.equals("enter")){

        }else if(type.equals("fail"))
        {
        }
    }

    public static String getPath(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[] {
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {
            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;

    }


    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context The context.
     * @param uri The Uri to query.
     * @param selection (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    public static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    public String getImagePath(Uri uri){
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        String document_id = cursor.getString(0);
        document_id = document_id.substring(document_id.lastIndexOf(":")+1);
        cursor.close();

        cursor = getContentResolver().query(
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
        cursor.moveToFirst();
        String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
        cursor.close();

        return path;
    }

    public void fileChooser(){

        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_GET_CONTENT);
        String type = "*/*";
        intent.setType(type);
        startActivityForResult(intent,FILE_SELECT_CODE);

    }

    public static void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }




    @Override
    public void onSingUptInteraction(String type) {

        if(type.equals("login")){

            Fragment logFragment = LoginFragment.newInstance(null,null);
            changeFragment(logFragment,true);

        }else if(type.equals("Error")) {
            Toast.makeText(this, "Failed", Toast.LENGTH_SHORT).show();
            Fragment logFragment = LoginFragment.newInstance(null,null);
            changeFragment(logFragment,true);
        }else if(type.equals("Creat")){
            Toast.makeText(this, "User created", Toast.LENGTH_SHORT).show();
            Fragment logFragment = LoginFragment.newInstance(null,null);
            changeFragment(logFragment,true);
        }

    }

    @Override
    public void onFragmentInteraction(final DummyContent.DummyItem item, View v, Integer idClick) {

        if(idClick == SHORT_CLICK) {
            Snackbar.make(v, "\"" + item.content + "\"", Snackbar.LENGTH_LONG)
                    .setAction(getResources().getString(R.string.press_action), new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            mBuilder.setProgress(0, 0, true);
                            mBuilder.setOngoing(true);
                            new Download(_server,_port,_user,_pass, item.content).execute();
                            mNotifyManager.notify(1, mBuilder.build());
                            mBuilder.setContentText("Download complete").setProgress(0,0,false);
                            mBuilder.setOngoing(false);
                            mNotifyManager.notify(1, mBuilder.build());
                        }
                    }).show();

            //TODO: Update fragment

            DocumentFragment fragment = DocumentFragment.newInstance(3);
            try {

                listFiles = (String[]) new Ftp(_user,_port,_server,_pass).execute().get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
            DummyContent
                    .setItemsList
                            (listFiles);
            changeFragment(fragment, true);


        }else if(idClick == LONG_CLICK){
            Snackbar.make(v, "\"" + item.content + "\"", Snackbar.LENGTH_LONG)
                    .setAction(getResources().getString(R.string.long_press_action), new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            new DeleteFile(_server,_port,_user,_pass,item.content).execute();
                        }
                    }).show();

            DocumentFragment fragment = DocumentFragment.newInstance(3);
            try {
                listFiles = (String[]) new Ftp(_user,_port,_server,_pass).execute().get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
            DummyContent
                    .setItemsList
                            (listFiles);
            changeFragment(fragment, true);
        }
    }

    @Override
    public void onFragmentInteractionMyProfile(String order,
                                               String password,
                                               boolean state) {
        if(order.equals(SAVEBACK)){

            getSupportActionBar().setTitle(R.string.yourFiles);
            getSupportActionBar().show();
            btn_upload.show();
            DocumentFragment fragment = DocumentFragment.newInstance(3);
            try {
                listFiles = (String[]) new Ftp(_user,_port,_server,_pass).execute().get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
            DummyContent
                    .setItemsList
                            (listFiles);
            changeFragment(fragment, true);
        }else if(order.equals(BACK)){


            getSupportActionBar().setTitle(R.string.yourFiles);
            getSupportActionBar().show();
            btn_upload.show();
            DocumentFragment fragment = DocumentFragment.newInstance(3);
            try {
                listFiles = (String[]) new Ftp(_user,_port,_server,_pass).execute().get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
            DummyContent
                    .setItemsList
                            (listFiles);
            changeFragment(fragment, true);
        }

    }

}
