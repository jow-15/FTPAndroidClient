package escoladeltreball.org.ftpandroidclient;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import escoladeltreball.org.ftpandroidclient.DocumentFragment.OnFragmentInteractionListener;
import escoladeltreball.org.ftpandroidclient.DummyContent.DummyItem;

//import com.example.iam07264872.recycleview_dem01.ItemFragment.OnListFragmentInteractionListener;

public class MyDocumentRecyclerViewAdapter extends RecyclerView.Adapter<MyDocumentRecyclerViewAdapter.ViewHolder> {

    private final List<DummyItem> mValues;
    private final OnFragmentInteractionListener mListener;

    public MyDocumentRecyclerViewAdapter(List<DummyItem> items,
                                         OnFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_document, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        String name = mValues.get(position).content;
        int imageR = R.drawable.unknown;
        name = (getFileWoutExt(name));

        holder.mIdView.setText(name);

        imageR = defineImage(mValues.get(position).content);


        holder.mContentView.setImageResource(imageR);


        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onFragmentInteraction(holder.mItem, v, MainActivity.SHORT_CLICK);

                }


            }
        });

        holder.mView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onFragmentInteraction(holder.mItem, view, MainActivity.LONG_CLICK);
                }

                return true;
            }
        });
    }

    private int defineImage(String ext) {
        switch (fileType(ext)){
            case MainActivity.TIPO_IMAGEN:
                return R.drawable.picture;
            case MainActivity.TIPO_AUDIO:
                return  R.drawable.sound4;
            case MainActivity.TIPO_DOCUMENTO:
                return R.drawable.document;
            case MainActivity.TIPO_SISTEMA:
                return R.drawable.system;
            default:
                return R.drawable.unknown;

        }

    }


    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mIdView;
        public final ImageView mContentView;


        public DummyItem mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mIdView = (TextView) view.findViewById(R.id.id);

            mContentView = (ImageView) view.findViewById(R.id.content);


        }

        @Override
        public String toString() {
            return super.toString() + " '" + "'";
        }
    }

    private int fileType(String path) {
        String ext = getFileExt(path);
        int type = 0;

        switch (ext) {
            case "jpg":
            case "png":
            case "bmp":
            case "gif":
            case "psd":
            case "ai":
            case "cdr":
            case "img":
            case "dwg":
            case "svg":
            case "ico":
                // "Imagen";
                type = 1;
                break;
            case "mp3":
            case "mid":
            case "midi":
            case "wav":
            case "wma":
            case "cda":
            case "ogg":
            case "ogm":
            case "aac":
            case "ac3":
            case "flac":
            case "mp4":
            case "aym":
            case "avi":
            case "mpeg":
                // "Audio";
                type =2;
                break;
            case "doc":
            case "odt":
            case "ott":
            case "fodt":
            case "uot":
            case "docx":
            case "xml":
            case "html":
            case "rtf":
            case "txt":
            case "dot":
                //"Document";
                type = 3;
                break;
            case "apk":
            case "bat":
            case "json":
            case "rpk":
                //"System";
                type =  4;
                break;
            default:
                //"Unknown";
                type = 5;
                break;
        }

        return  type;
    }
    public static String getFileExt(String fileName) {
        return fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length());
    }

    public static String getFileWoutExt(String fileName) {
        return fileName.substring(0, fileName.lastIndexOf("."));
    }



}
