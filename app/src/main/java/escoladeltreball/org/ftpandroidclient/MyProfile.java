package escoladeltreball.org.ftpandroidclient;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;

import butterknife.ButterKnife;
import butterknife.InjectView;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MyProfile.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MyProfile#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MyProfile extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "param3";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private String mParam3;




    @InjectView(R.id.et_name)
    EditText _editName;
    @InjectView(R.id.password)
    EditText _editPassword;
    @InjectView(R.id.premium)
    Switch _upgrade;
    @InjectView(R.id.bt_save)
    Button _saveButton;

    @InjectView(R.id.bt_back)
    Button _backButton;


    private OnFragmentInteractionListener mListener;

    public MyProfile() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MyProfile.
     */
    // TODO: Rename and change types and number of parameters
    public static MyProfile newInstance(String param1, String param2, String param3) {
        MyProfile fragment = new MyProfile();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        args.putString(ARG_PARAM3, param3);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
            mParam3 = getArguments().getString(ARG_PARAM3);
        }



    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_my_profile, container, false);
        ButterKnife.inject(this,view);

        //Initial states
        _editName.setText(mParam1, EditText.BufferType.NORMAL);
        _editName.setTextIsSelectable(false);
        _editName.setFocusable(false);


        _editPassword.setText(mParam2, EditText.BufferType.NORMAL);

        if(mParam3.equals(MainActivity.TIPO_NORMAL)){
            _upgrade.setChecked(false);


        }else if(mParam3.equals(MainActivity.TIPO_PREMIUM)){
            _upgrade.setChecked(true);

            }

            _upgrade.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    // do something, the isChecked will be
                    // true if the switch is in the On position
                    if(isChecked){
                        Snackbar.make(buttonView, getResources().getString(R.string.upgrade), Snackbar.LENGTH_SHORT).show();

                    }else{
                        Snackbar.make(buttonView, getResources().getString(R.string.Degraded), Snackbar.LENGTH_SHORT).show();

                    }
                }
            });


        _saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                saving();

                mListener.onFragmentInteractionMyProfile(
                        MainActivity.SAVEBACK,
                        _editPassword.getText().toString(),
                        _upgrade.isChecked());



            }
        });

        _backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mListener.onFragmentInteractionMyProfile(
                        MainActivity.BACK,
                        _editPassword.getText().toString(),
                        _upgrade.isChecked());



            }
        });



        return view;
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void saving() {


       _saveButton.setEnabled(false);
        _backButton.setEnabled(false);

        final ProgressDialog progressDialog = new ProgressDialog(getContext(),
                R.style.AppTheme_Dark_Dialog);

        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false); // Per tal de que no sigui possible saltar-se la animació
        progressDialog.setMessage(getResources().getString(R.string.saving));
        progressDialog.show();


        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {

                        progressDialog.dismiss();
                    }
                }, 1500);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteractionMyProfile(String order,
                                            String newPassword,
                                            boolean state);
    }
}
