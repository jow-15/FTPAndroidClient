package escoladeltreball.org.ftpandroidclient;

import android.os.AsyncTask;

import io.swagger.client.ApiClient;
import io.swagger.client.api.SigninApi;
import io.swagger.client.model.NewUser;

/**
 * Created by root on 6/13/17.
 */

public class SignIn extends AsyncTask {

    private String nick;
    private String password;
    private String type;

    public SignIn(String nick, String password, String type) {
        this.nick = nick;
        this.password = password;
        this.type = type;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    protected Object doInBackground(Object[] params) {

        try {
            SigninApi apiInstance = new SigninApi();
            ApiClient pc = new ApiClient();
            pc.setBasePath("http://10.0.2.2:8081/v1/");
            apiInstance.setApiClient(pc);
            NewUser nu = new NewUser();
            nu.setNick(nick);
            nu.setPassword(password);
            nu.setType(type);
            apiInstance.newUser(nu);

        } catch (Exception e) {
            e.printStackTrace();

            return "Error";

        }

        return null;
    }
}
