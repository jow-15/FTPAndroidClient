package escoladeltreball.org.ftpandroidclient;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.ButterKnife;
import butterknife.InjectView;


public class SignInFragment extends Fragment {

    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";


    @InjectView(R.id.input_name)EditText _nameText;
    @InjectView(R.id.input_password) EditText _passText;
    @InjectView(R.id.btn_signup)Button _signupButton;
    @InjectView(R.id.link_login)TextView _loginLink;

    private String mParam1;
    private String mParam2;
    private RadioGroup radioTypeGroup;
    private RadioButton radioTypeButton;


    private OnSignUpInteractionListener mListener;

    public SignInFragment() {
        // Required empty public constructor
    }


    public static SignInFragment newInstance(String param1, String param2) {
        SignInFragment fragment = new SignInFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_sign_in, container, false);

        ButterKnife.inject(this,view);

        radioTypeGroup=(RadioGroup)view.findViewById(R.id.user_type);

        _loginLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onLoginPressed();

            }
        });

        return view;
    }

    @Override
    public void onViewCreated(final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        _signupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int selectedId=radioTypeGroup.getCheckedRadioButtonId();
                radioTypeButton=(RadioButton)view.findViewById(selectedId);
                //Toast.makeText(getContext(),radioTypeButton.getText(),Toast.LENGTH_SHORT).show();
                signup();
                //finish();
            }
        });
    }

    public void onCreatePressed() {
        if (mListener != null) {
            mListener.onSingUptInteraction("create");
        }
    }

    public void onLoginPressed() {
        if (mListener != null) {
            mListener.onSingUptInteraction("login");
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnSignUpInteractionListener) {
            mListener = (OnSignUpInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnSignUpInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public void signup() {

        _signupButton.setEnabled(false);

        final ProgressDialog progressDialog = new ProgressDialog(getContext(),
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Creating Account...");
        progressDialog.show();

        final String name = _nameText.getText().toString();
        final String password = _passText.getText().toString();

        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        System.out.println("HOLA AquÇIO!!!");
                        System.out.println("usuario es: "+name);
                        System.out.println("password es: "+password);
                        System.out.println("tipo es "+radioTypeButton.getText());
                        if(radioTypeButton.getText().equals("Free User")){

                           if( new SignIn(name,password,"1").execute().equals(new String("Error"))){
                               mListener.onSingUptInteraction("Error");
                           }else{
                               mListener.onSingUptInteraction("Creat");

                           }

                        }else if(radioTypeButton.getText().equals("Prenium")){
                            if( new SignIn(name,password,"2").execute().equals(new String("Error"))){
                                mListener.onSingUptInteraction("Error");
                            }else{
                                mListener.onSingUptInteraction("Creat");

                            }

                        }

                        progressDialog.dismiss();
                    }

                }, 3000);
    }

    public void onSignupSuccess() {
        _signupButton.setEnabled(true);
        //setResult(RESULT_OK, null);

    }

    public interface OnSignUpInteractionListener {

        void onSingUptInteraction(String type);
    }
}
